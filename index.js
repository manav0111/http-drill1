const http=require('http');
const { type } = require('os');
const uuid4=require('uuid4');


const server=http.createServer((req,res)=>{

    if(req.url=="/html")
    {
        res.end(`<!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
              <p> - Martin Fowler</p>
        
          </body>
        </html>`);
    }
    else if(req.url=="/json")
    {
        res.end(JSON.stringify({
            "slideshow": {
              "author": "Yours Truly",
              "date": "date of publication",
              "slides": [
                {
                  "title": "Wake up to WonderWidgets!",
                  "type": "all"
                },
                {
                  "items": [
                    "Why <em>WonderWidgets</em> are great",
                    "Who <em>buys</em> WonderWidgets"
                  ],
                  "title": "Overview",
                  "type": "all"
                }
              ],
              "title": "Sample Slide Show"
            }
          }));
    }
    else if(req.url=="/uuid") {
        let uuid=uuid4();
        res.end(JSON.stringify({'uuid':uuid}));
    }
    else if(req.url.includes("/status"))
    {   
        const statusCode=req.url.split('/')[2];
        res.end(JSON.stringify({'statusCode':statusCode}));
    }
    else if(req.url.includes("/delay"))
    {
        const delayinseconds=Number(req.url.split('/')[2]);
        console.log(delayinseconds);
        console.log(typeof delayinseconds);

        setTimeout(()=>{
            res.end(JSON.stringify({'success':`After ${delayinseconds} miliseconds`}));
        },delayinseconds*1000);

        // // res.end("hello");

    }


})


server.listen(3000,()=>{
    console.log("Server is running on 3000");
})